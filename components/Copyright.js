import { Text } from "@rneui/themed"
import { StyleSheet, View } from "react-native"

export default Copyright = ({ style }) => {
  return (
    <View style={[style]}>
      <Text style={defaultStyle.copyright}>Mockup Design by
        <Text style={[defaultStyle.copyright, {fontWeight: 'bold'}]}> Surya Maresa</Text>
      </Text>
    </View>
  )
}

const defaultStyle = StyleSheet.create({
  copyright: {
    textAlign: 'center',
    color: 'white',
    fontSize: 12,
    marginTop: 10,
  }
})