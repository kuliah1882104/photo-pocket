export default categories = [
  {
    title: 'Photography', 
    image: require('./images/categories/photography.png'),
    route: 'Photography',
  },
  {
    title: 'Videography', 
    image: require('./images/categories/videography.png'),
    route: null,
  },
  {
    title: 'Photo Studio', 
    image: require('./images/categories/photo_studio.png'),
    route: 'PhotoStudio',
  },
  {
    title: 'Camera Recomendation', 
    image: require('./images/categories/camera_service.png'),
    route: 'Recomendation',
  },
  {
    title: 'Estimate Your Gear Set Up', 
    image: require('./images/categories/estimate_setup.png'),
    route: null,
  },
  {
    title: 'Commercial Shoot', 
    image: require('./images/categories/commercial_shoot.png'),
    route: null,
  },
  {
    title: 'Equipment Rent', 
    image: require('./images/categories/equipment_rent.png'),
    route: null,
  },
]