import { Button, Input } from "@rneui/themed";
import { useContext, useState } from "react";
import { ImageBackground, StyleSheet, Text, View } from "react-native";
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { AuthContext } from "../navigation/auth";

export default RegisterScreen = () => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const { signUp } = useContext(AuthContext)

  return (
    <ImageBackground resizeMode="cover" style={{ flex: 1 }} source={require('../assets/images/register.jpg')} >
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.textContainer}>
            <Text style={[styles.title]}>Photo.Pocket</Text>
          </View>
          <View style={styles.authContainer}>
            <Text style={[styles.subtitle]}>Sign Up</Text>
            <Input
              containerStyle={{ height: 55 }}
              inputContainerStyle={{ borderBottomWidth: 0, paddingBottom: 0 }}
              inputStyle={styles.input}
              placeholder="Your Name"
              value={name}
              onChangeText={setName}
            />
            <Input
              containerStyle={{ height: 55 }}
              inputContainerStyle={{ borderBottomWidth: 0, paddingBottom: 0 }}
              inputStyle={styles.input}
              placeholder="Email"
              value={email}
              onChangeText={setEmail}
              autoCapitalize="none"
            />
            <Input
              inputContainerStyle={{ borderBottomWidth: 0 }}
              inputStyle={styles.input}
              secureTextEntry
              placeholder="Password"
              value={password}
              onChangeText={setPassword}
              autoCapitalize="none"
            />
            <Button buttonStyle={styles.button} title="Submit" onPress={() => signUp({ name, email, password })} />
          </View>
          <View></View>
        </View>
      </View>
    </ImageBackground >

  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  backgroundImage: {
    height: '100%',
    width: '100%',
    position: 'absolute',
  },
  content: {
    padding: 20,
    paddingBottom: 40,
    flex: 1,
    justifyContent: 'space-between',
  },
  textContainer: {
    marginTop: wp(20),
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(5),
    marginBottom: 30,
  },
  subtitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(10),
    marginBottom: 20,
  },
  authContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: wp(10),
  },
  input: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    borderBottomWidth: 0,
    borderRadius: 15,
    paddingHorizontal: 20,
    paddingVertical: 8,
    color: 'white',
  },
  button: {
    width: 150,
    borderRadius: 90,
    backgroundColor: 'rgba(0, 0, 0, 1)'
  }
})