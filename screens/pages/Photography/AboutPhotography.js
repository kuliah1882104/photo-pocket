import { ImageBackground, ScrollView, StyleSheet, Text, View } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Layout from "../Layout";

export default AboutPhotography = () => {
  return (
    <Layout>
      <ScrollView style={styles.content}>
        <Text style={styles.subtitle}>About Photography</Text>
        <View style={{ marginTop: -45 }}>
          <ImageBackground style={styles.image} resizeMode="contain" source={require('../../../assets/images/login.jpg')} />
          <Text style={styles.paragraph}>
            Fotografi adalah seni dan ilmu menangkap gambar melalui cahaya.
            Dasar-dasar fotografi mencakup pemahaman tentang pencahayaan, komposisi, fokus, dan penggunaan warna.
            Pencahayaan adalah elemen kunci dalam fotografi yang terdiri dari tiga komponen utama: aperture, shutter speed dan ISO.
            Aperture mengontrol seberapa besar bukaan lensa yang memungkinkan cahaya masuk, dan ini diukur dalam f-stop.
            Shutter speed menentukan seberapa lama rana kamera terbuka untuk membiarkan cahaya masuk, mempengaruhi kemampuan untuk membekukan atau menangkap gerakan.
            ISO mengatur sensitivitas sensor kamera terhadap cahaya, dimana ISO rendah memberikan gambar lebih bersih dengan sedikit noise,
            sementara ISO tinggi memungkinkan pemotretan dalam kondisi cahaya rendah namun dengan peningkatan noice.
          </Text>
        </View>
      </ScrollView>
    </Layout>
  )
}

const styles = StyleSheet.create({
  subtitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(8),
    marginTop: hp(1),
    textAlign: 'center'
  },
  content: {
    paddingHorizontal: 20,
  },
  image: {
    aspectRatio: 1,
    borderRadius: 90,
  },
  paragraph: {
    color: 'white',
    textAlign: 'justify',
    marginTop: -40,
  },
})