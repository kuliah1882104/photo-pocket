import { Text } from "@rneui/themed";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Layout from "../Layout";
import { useNavigation } from "@react-navigation/native";

export default Photography = () => {
  const navigation = useNavigation()

  return (
    <Layout>
      <View>
        <Text style={styles.subtitle}>Photography</Text>
        <View style={styles.content}>
          <TouchableOpacity style={styles.subPageButton} onPress={() => navigation.navigate('Photography/About')}>
            <Text style={styles.textPage}>About Photography</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.subPageButton}>
            <Text style={styles.textPage}>Photography Technic</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.subPageButton}>
            <Text style={styles.textPage}>Rules of Third</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.subPageButton}>
            <Text style={styles.textPage}>Gear</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.subPageButton}>
            <Text style={styles.textPage}>Link Tutorial</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Layout>
  )
}

const styles = StyleSheet.create({
  subtitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(10),
    marginBottom: 50,
    textAlign: 'center'
  },
  content: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    paddingVertical: 50,
  },
  subPageButton: {
    paddingVertical: 5,
  },
  textPage: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: wp(5.5),
  },
})