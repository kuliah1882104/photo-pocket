import { Text } from '@rneui/themed'
import { Dimensions, ImageBackground, ScrollView, StyleSheet } from 'react-native'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { SafeAreaView } from 'react-native-safe-area-context'
import Copyright from '../../components/Copyright'


export default PageLayout = ({ children }) => {
  const windowHeight = Dimensions.get('screen').height

  return (
    <ImageBackground source={require('../../assets/images/welcome.jpg')} resizeMode="cover" style={styles.container}>
      <SafeAreaView style={[styles.content, { height: windowHeight }]}>
        <Text style={styles.title}>Photo.Pocket</Text>
        {children}
        <Copyright />
      </SafeAreaView>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  content: {
    paddingVertical: 50,
    justifyContent: 'space-between',
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(5),
    marginBottom: 15,
    textAlign: 'center'
  },
})