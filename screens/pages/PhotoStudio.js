import { Dimensions, ImageBackground, ScrollView, StyleSheet, Text, View } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SafeAreaView } from "react-native-safe-area-context";
import Copyright from "../../components/Copyright";

export default PhotoStudio = () => {
  const windowHeight = Dimensions.get('screen').height

  return (
    <ImageBackground source={require('../../assets/images/welcome.jpg')} resizeMode="cover" style={styles.container}>
      <SafeAreaView style={[styles.content, { height: windowHeight }]}>
        <ScrollView>
          <View>
            <Text style={styles.title}>Photo.Pocket</Text>
            <Text style={styles.subtitle}>Photo Studio</Text>
          </View>
          <View style={{ paddingHorizontal: 30, marginTop: -60 }}>
            <ImageBackground style={styles.image} resizeMode="contain" source={require('../../assets/images/categories/photo_studio.png')} />
          </View>
          <Text style={[styles.title, { marginTop: -50 }]}>Surya Studio</Text>
          <Text style={[styles.textContent, { marginTop: -10 }]}>Jl. Utama Bintaro No. 25, Bintaro South Tangerang</Text>
          <Text style={styles.textContent}>Contact Person: 0812 8888 0000</Text>
          <View style={{ paddingHorizontal: 30, marginTop: -10 }}>
            <ImageBackground style={styles.image} resizeMode="contain" source={require('../../assets/images/fujifilm_studio.jpg')} />
          </View>
          <Text style={[styles.title, { marginTop: -30 }]}>Fujifilm Studio</Text>
          <Text style={[styles.textContent, { marginTop: -5 }]}>Jl. Utama Bintaro Utara No. 99, Bintaro South Tangerang</Text>
          <Text style={styles.textContent}>Contact Person: 0812 8888 0000</Text>

          <Copyright style={{ marginTop: 50 }} />
        </ScrollView>
      </SafeAreaView>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  content: {
    paddingHorizontal: 30,
    paddingVertical: 50,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(5),
    marginBottom: 15,
    textAlign: 'center'
  },
  subtitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(10),
    marginBottom: 10,
    textAlign: 'center'
  },
  image: {
    aspectRatio: 1,
    // paddingHorizontal: 20,
    borderRadius: 90,
    // width: '50%',
    // flex: 1,
    // height: 130,
    // width: windowWidth
  },
  textContent: {
    color: 'white',
    textAlign: 'center',
    fontWeight: '00',
    marginBottom: 0,
  }
})