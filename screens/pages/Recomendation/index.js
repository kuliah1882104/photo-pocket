import { Picker } from "@react-native-picker/picker";
import { useNavigation } from "@react-navigation/native";
import { Button, Text } from "@rneui/themed";
import { useState } from "react";
import { StyleSheet, View } from "react-native";
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Layout from "../Layout";

export default Recomendation = () => {
  const [priceRange, onSelectPriceRange] = useState(0)
  const [maxISO, onSelectMaxISO] = useState(0)
  const [megapixel, onSelectMegapixel] = useState(0)

  const navigation = useNavigation()

  const goResult = () => {
    navigation.navigate('Recomendation/Result', {priceRange, maxISO, megapixel})
  }

  return (
    <Layout>
      <View>
        <Text style={styles.subtitle}>Camera Recomendation</Text>
        <View style={styles.content}>
          <Text style={styles.textPage}>Masukkan Bobot</Text>
          <View style={{ paddingHorizontal: 20 }}>
            <Picker
              selectedValue={priceRange}
              onValueChange={(value, index) => onSelectPriceRange(value)}
              style={styles.filterContainer}
              dropdownIconColor="white"
            >
              <Picker.Item label="Kriteria Harga" value={0} />
              <Picker.Item label="Di bawah Rp15.000.000" value={1} />
              <Picker.Item label="Rp15.000.000 - Rp30.000.000" value={2} />
              <Picker.Item label="Rp30.000.000 - Rp50.000.000" value={3} />
              <Picker.Item label="Rp50.000.000 - Rp75.000.000" value={3} />
              <Picker.Item label="Di atas Rp75.000.000" value={4} />
            </Picker>
            <Picker
              selectedValue={maxISO}
              onValueChange={(value, index) => onSelectMaxISO(value)}
              style={styles.filterContainer}
              dropdownIconColor="white"
            >
              <Picker.Item label="ISO Max" value={0} />
              <Picker.Item label="ISO 12800" value={12800} />
              <Picker.Item label="ISO 25600" value={25600} />
              <Picker.Item label="ISO 32000" value={32000} />
              <Picker.Item label="ISO 40000" value={40000} />
              <Picker.Item label="ISO 51200" value={51200} />
            </Picker>
            <Picker
              selectedValue={megapixel}
              onValueChange={(value, index) => onSelectMegapixel(value)}
              style={styles.filterContainer}
              dropdownIconColor="white"
            >
              <Picker.Item label="Resolusi Gambar" value={0} />
              <Picker.Item label="12 MP" value={12} />
              <Picker.Item label="20 MP" value={20} />
              <Picker.Item label="25 MP" value={25} />
              <Picker.Item label="35 MP" value={35} />
              <Picker.Item label="40 MP" value={40} />
            </Picker>
            <Button title="Hitung" containerStyle={{ marginTop: 15 }} onPress={goResult} />
          </View>
        </View>
      </View>
    </Layout>
  )
}

const styles = StyleSheet.create({
  subtitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(8),
    marginBottom: 50,
    textAlign: 'center'
  },
  content: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    paddingVertical: 50,
  },
  subPageButton: {
    paddingVertical: 5,
  },
  textPage: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: wp(5.5),
  },
  filterContainer: {
    color: 'white',
    marginTop: 15,
    backgroundColor: 'rgba(0, 0, 0, 0.8)'
  },
})