import { Picker } from "@react-native-picker/picker";
import { useNavigation } from "@react-navigation/native";
import { Button, Text } from "@rneui/themed";
import { useEffect, useState } from "react";
import { ActivityIndicator, FlatList, ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Layout from "../Layout";
import { useQuery } from "react-query";
import { getCameras } from "../../../api/camera";
import defaultStyle from "../../../theme";
import DataTable, { COL_TYPES } from "react-native-datatable-component";

export default RecomendationResult = ({ route, navigation }) => {
  const { userToken, maxISO, megapixel, priceRange } = route.params

  const { isLoading, isError, error, data } = useQuery({
    queryKey: ['cameras', priceRange, maxISO, megapixel],
    queryFn: () => getCameras(userToken, maxISO, megapixel, priceRange)
  })

  const renderHeader = () => {
    return (
      <View style={styles.row}>
        <Text style={styles.headerCell}>Model</Text>
        <Text style={styles.headerCell}>Score</Text>
      </View>
    );
  };

  return (
    <Layout>
      <ScrollView>
        <Text style={styles.subtitle}>Camera Recomendation</Text>
        <View style={styles.content}>
          <Text style={styles.textPage}>Hasil Perhitungan</Text>
          {isLoading ? (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <ActivityIndicator animating={true} color="rgb(14 165 233)" />
            </View>
          ) : isError ? (
            <Text style={defaultStyle.textWhite}>Error Loading Data: {error?.message}</Text>
          ) : (
            <>
              {data.length === 0 ? (
                <Text style={defaultStyle.textWhite}>Tidak ada hasil yang sesuai.</Text>
              ) : (
                <View style={{width: '90%', alignSelf: 'center', marginTop: 10}}>
                  {renderHeader()}
                  {data.map((item, index) => (
                    <TouchableOpacity style={styles.row} key={index} onPress={() => navigation.navigate('Recomendation/Detail', {cameraID: item._id})}>
                      <Text style={styles.cell}>{item.brand} {item.model}</Text>
                      <Text style={styles.cell}>{Math.ceil(item.preference_value*10000) / 10000}</Text>
                    </TouchableOpacity>
                  ))}
                </View>
              )}
            </>
          )}
        </View>
      </ScrollView>
    </Layout>
  )
}

const styles = StyleSheet.create({
  subtitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(8),
    marginBottom: 50,
    textAlign: 'center'
  },
  content: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    paddingVertical: 50,
  },
  subPageButton: {
    paddingVertical: 5,
  },
  textPage: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: wp(5.5),
  },
  filterContainer: {
    color: 'white',
    marginTop: 15,
    backgroundColor: 'rgba(0, 0, 0, 0.8)'
  },
  tableContainer: {
    flex: 1,
    padding: 16,
    backgroundColor: '#fff',
  },
  row: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
    paddingVertical: 8,
  },
  headerCell: {
    flex: 1,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
  },
  cell: {
    flex: 1,
    textAlign: 'center',
    color: 'white',
  },
})