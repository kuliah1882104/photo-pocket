import { SimpleImageSlider } from "@one-am/react-native-simple-image-slider"
import { Card } from "@rneui/themed"
import { ActivityIndicator, Dimensions, ImageBackground, StyleSheet, Text, View } from "react-native"
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { SafeAreaView } from "react-native-safe-area-context"
import { useQuery } from "react-query"
import { getCameraByID } from "../../../api/camera"
import Copyright from "../../../components/Copyright"

export default Detail = ({ route, navigation }) => {
  const { userToken, cameraID } = route.params
  const windowHeight = Dimensions.get('screen').height

  const { isLoading, isError, error, data } = useQuery({
    queryKey: ['cameras', cameraID],
    queryFn: () => getCameraByID(userToken, cameraID)
  })

  const rupiah = number => {
    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR'
    }).format(number)
  }

  return (
    <ImageBackground source={require('../../../assets/images/welcome.jpg')} resizeMode="cover" style={styles.container}>
      <SafeAreaView style={[styles.content, { height: windowHeight }]}>
        {isLoading ? (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator animating={true} color="rgb(14 165 233)" />
          </View>
        ) : isError ? (
          <Text>Error Loading Data: {error?.message}</Text>
        ) : (
          <Card containerStyle={{ backgroundColor: 'transparent', }}>
            <Card.Title style={{ color: 'white', fontSize: 20 }}>{data.brand} {data.model}</Card.Title>
            <SimpleImageSlider
              data={data?.images?.map((image, index) => ({
                source: image,
                key: index.toString()
              }))}
              fullScreenEnabled={true}
              renderFullScreenDescription={(_, index) => (
                <Text style={{ color: '#ffffff' }}>Image {index + 1}</Text>
              )}
            />
            <View style={{ marginTop: 10 }}>
              <View style={styles.specContainer}>
                <Text style={styles.specName}>Megapixels</Text>
                <Text style={styles.specValue}>{data.megapixels}</Text>
              </View>
              <View style={styles.specContainer}>
                <Text style={styles.specName}>Aperture</Text>
                <Text style={styles.specValue}>{data.aperture}</Text>
              </View>
              <View style={styles.specContainer}>
                <Text style={styles.specName}>Shutter Speed</Text>
                <Text style={styles.specValue}>{data.shutter_speed}</Text>
              </View>
              <View style={styles.specContainer}>
                <Text style={styles.specName}>Max ISO</Text>
                <Text style={styles.specValue}>{data.max_iso}</Text>
              </View>
              <View style={styles.specContainer}>
                <Text style={styles.specName}>LCD Size</Text>
                <Text style={styles.specValue}>{data.lcd_size} inch</Text>
              </View>
              <View style={styles.specContainer}>
                <Text style={styles.specName}>Video Resolution</Text>
                <Text style={styles.specValue}>{data.video_resolution}</Text>
              </View>
              <View style={styles.specContainer}>
                <Text style={styles.specName}>Weight</Text>
                <Text style={styles.specValue}>{data.weight} grams</Text>
              </View>
              <View style={styles.specContainer}>
                <Text style={styles.specName}>Sensor Size</Text>
                <Text style={styles.specValue}>{data.sensor_size}</Text>
              </View>
              <View style={styles.specContainer}>
                <Text style={styles.specName}>Battery Life</Text>
                <Text style={styles.specValue}>{data.battery_life} shots</Text>
              </View>
              <View style={styles.specContainer}>
                <Text style={styles.specName}>Price</Text>
                <Text style={styles.specValue}>{rupiah(data.price)}</Text>
              </View>
            </View>
          </Card>
        )}
        <Copyright style={{ marginTop: -20 }} />
      </SafeAreaView>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
  },
  content: {
    justifyContent: 'space-between',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(5),
    marginVertical: 15,
    textAlign: 'center'
  },
  specContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
    paddingVertical: 8,
  },
  specName: {
    color: 'white',
    fontWeight: 'bold',
  },
  specValue: {
    color: 'white',
  },
})