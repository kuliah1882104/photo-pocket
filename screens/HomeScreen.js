import { Entypo } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { ImageBackground, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import categories from '../assets/categories';

export default HomeScreen = () => {
  const navigation = useNavigation()

  const gotoPage = page => {
    if (page !== null) {
      navigation.navigate(page)
    }
  }

  return (
    <ImageBackground resizeMode="cover" style={{ flex: 1 }} source={require('../assets/images/login.jpg')} >
      <View style={styles.container}>
        <ScrollView style={styles.content} stickyHeaderIndices={0}>
          <View style={styles.headerContainer}>
            <Text style={[styles.subtitle]}>Photo.Pocket</Text>
            <View style={styles.searchContainer}>
              <TextInput
                placeholder="Search"
                placeholderTextColor="gray"
                style={styles.input}
              />
              <Entypo name="magnifying-glass" color="white" size={20} />
            </View>
          </View>
          <View style={styles.categoriesContainer}>
            {categories.map((category, index) => (
              <TouchableOpacity key={index} onPress={() => gotoPage(category.route)}>
                <ImageBackground resizeMode="cover" source={category.image} style={styles.category} imageStyle={styles.categoryImage} >
                  <View style={styles.categoryTextContainer}>
                    <Text style={styles.categoryText}>{category.title}</Text>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>
      </View>
    </ImageBackground >
  )
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // justifyContent: 'space-between',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  content: {
    padding: 20,
    // paddingBottom: 40,
    // flex: 1,
    // justifyContent: 'flex-start',
  },
  headerContainer: {
    marginTop: wp(20),
    // marginBottom: 10,
    paddingHorizontal: 10,
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(5),
    marginBottom: 30,
    textAlign: 'center'
  },
  subtitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(10),
    marginBottom: 10,
    textAlign: 'center'
  },
  authContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: wp(10),
  },
  searchContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    borderRadius: 15,
    borderBottomWidth: 0,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,

  },
  input: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 8,
    color: 'white',
    letterSpacing: 1.25,
  },
  button: {
    width: 150,
    borderRadius: 90,
    backgroundColor: 'rgba(0, 0, 0, 1)'
  },
  categoriesContainer: {
    marginVertical: 20,
    paddingHorizontal: 20,
  },
  category: {
    flex: 1,
    justifyContent: 'center',
    height: 130,
    // borderRadius: 100,
    // paddingVertical: 50,
    // paddingHorizontal: 60,
    marginBottom: 25,
  },
  categoryImage: {
    borderRadius: 30,
  },
  categoryTextContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    height: '100%',
    borderRadius: 30,
    paddingHorizontal: 60,
  },
  categoryText: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 25,
  }
})