import { Image, StyleSheet, Text, View } from "react-native"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import defaultStyle from '../theme'
import { Button } from "@rneui/themed";
import { useNavigation } from "@react-navigation/native";

export default WelcomeScreen = () => {
  const navigation = useNavigation()

  return (
    <View style={styles.container}>
      <Image style={styles.backgroundImage} source={require('../assets/images/welcome.jpg')} />
      <View style={styles.content}>
        <View style={styles.textContainer}>
          <Text style={[styles.title]}>Photo.Pocket</Text>
          <Text style={[styles.subtitle]}>Welcome</Text>
        </View>
        <View style={styles.authContainer}>
          <Text style={[defaultStyle.textWhite]}>Already have an account</Text>
          <Button title="Sign In" buttonStyle={styles.button} onPress={() => navigation.navigate('Login')} />
          <Text style={[defaultStyle.textWhite, {marginTop: 10}]}>Don't have account? Please sign up</Text>
          <Button title="Sign Up" buttonStyle={styles.button} onPress={() => navigation.navigate('Register')} />

          <Text style={[defaultStyle.textWhite, {marginTop: wp(20)}]}>
            Mockup Design by <Text style={[defaultStyle.textWhite, {fontWeight: 'bold'}]}>Surya Maresa</Text>
          </Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  backgroundImage: {
    height: '100%',
    width: '100%',
    position: 'absolute',
  },
  content: {
    padding: 20,
    paddingBottom: 40,
    flex: 1,
    justifyContent: 'space-between',
  },
  textContainer: {
    marginTop: wp(20),
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(5),
    marginBottom: 30,
  },
  subtitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: wp(10),
  },
  authContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    rowGap: 5,
  },
  button: {
    width: 150,
    borderRadius: 90,
    backgroundColor: 'rgba(39, 39, 39, 0.5)'
  }
})