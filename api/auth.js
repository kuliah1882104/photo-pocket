import request from "./request";

export const signIn = ({ email, password }) =>
  request({
    url: '/api/login',
    method: 'POST',
    data: {
      email: email,
      password: password,
    }
  })

export const signUp = ({ name, email, password }) =>
  request({
    url: '/api/register',
    method: 'POST',
    data: {
      name: name,
      email: email,
      password: password,
    }
  })

export const user = (token) =>
  request({
    url: '/api/user',
    method: 'GET',
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
