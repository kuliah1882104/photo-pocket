import request from "./request";

export const getCameras = (userToken, maxISO, megapixel, priceRange) => {
  let url = '/api/cameras?'

  if (maxISO !== null && maxISO !== '' && maxISO !== 0) {
    url += `&max_iso=${maxISO}`
  }

  if (megapixel !== null && megapixel !== '' && megapixel !== 0) {
    url += `&megapixels=${megapixel}`
  }

  const priceRanges = {
    1: ['0', '15000000'],
    2: ['15000000', '30000000'],
    3: ['30000000', '50000000'],
    4: ['50000000', '75000000'],
    5: ['75000000', '7500000000'],
  }

  console.log('PRICE:', priceRange);

  if (priceRange !== 0) {
    url += `&price_range%5B%5D=${priceRanges[priceRange][0]}&price_range%5B%5D=${priceRanges[priceRange][1]}`
  }

  return request({
    url: url,
    method: 'GET',
    headers: {
      'Authorization': 'Bearer ' + userToken
    }
  })
}

export const getCameraByID = (userToken, id) => 
  request({
    url: `/api/cameras/${id}`,
    method: 'GET',
    headers: {
      'Authorization': 'Bearer ' + userToken
    }
  })