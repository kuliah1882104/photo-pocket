const baseURL = process.env.EXPO_PUBLIC_API_URL

export default async (options) => {
  const { url, method, headers, data } = options;

  const defaultHeaders = {
    'Accept': 'application/json',
  };

  if (method === 'POST') {
    defaultHeaders['Content-Type'] = 'application/json';
  }

  try {
    const response = await fetch(`${baseURL}${url}`, {
      method: method || 'GET',
      headers: {
        ...defaultHeaders,
        ...headers,
      },
      body: data ? JSON.stringify(data) : undefined,
    });

    const responseData = await response.json();

    if (!response.ok) {
      throw new Error(responseData?.message);
    }

    console.log(responseData);
    return responseData;
  } catch (error) {
    if (error instanceof TypeError) {
      console.log("ERROR_REQUEST");
      console.log(error.message);
    } else {
      console.log("ERROR_RESPONSE");
      console.log(error);
      console.log(error.message);
    }
    return Promise.reject(error);
  }
}
