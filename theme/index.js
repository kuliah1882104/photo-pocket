import { StyleSheet } from "react-native";

const defaultStyle = StyleSheet.create({
  textWhite: {
    color: 'white',
    fontWeight: '300',
  }
})

export default defaultStyle