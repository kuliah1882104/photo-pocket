import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as SecureStore from 'expo-secure-store';
import { useEffect, useMemo, useReducer } from "react";
import { Alert, Text } from 'react-native';
import { signIn as signInApi, signUp as signUpApi, user as userApi } from '../api/auth';
import HomeScreen from "../screens/HomeScreen";
import LoginScreen from "../screens/LoginScreen";
import RegisterScreen from "../screens/RegisterScreen";
import WelcomeScreen from "../screens/WelcomeScreen";
import PhotoStudio from "../screens/pages/PhotoStudio";
import Photography from "../screens/pages/Photography";
import AboutPhotography from "../screens/pages/Photography/AboutPhotography";
import Recomendation from "../screens/pages/Recomendation";
import RecomendationResult from "../screens/pages/Recomendation/Result";
import { AuthContext } from './auth';
import RecomendationDetail from "../screens/pages/Recomendation/Detail";

const Stack = createNativeStackNavigator()

export default AppNavigation = () => {
  const [state, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    }
  );

  useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await SecureStore.getItemAsync('userToken');
      } catch (e) {
        // Restoring token failed
        console.log(e);
      }

      userApi(userToken)
        .then(async res => {
          await SecureStore.setItemAsync('userName', res.name)
          await SecureStore.setItemAsync('userName', res.email)
          dispatch({ type: 'RESTORE_TOKEN', token: userToken });

        })
        .catch(err => {
          console.log(err);
          dispatch({ type: 'RESTORE_TOKEN', token: null });
        })
    };

    bootstrapAsync();
  }, []);

  const authContext = useMemo(
    () => ({
      signIn: async (data) => {
        signInApi(data)
          .then(async res => {
            if (res?.access_token) {
              await SecureStore.setItemAsync('userToken', res.access_token);
              dispatch({ type: 'SIGN_IN', token: res.access_token });
            } else {
              Alert.alert('Sign In Failed', err?.message || 'Something wrong')
            }
          })
          .catch(err => {
            console.log(err.message);
            Alert.alert('Sign In Failed', err?.message || 'Something wrong')
          })
      },
      signOut: () => dispatch({ type: 'SIGN_OUT' }),
      signUp: async (data) => {
        signUpApi(data)
          .then(res => {
            if (res?.access_token) {
              dispatch({ type: 'SIGN_IN', token: res.access_token });
            } else {
              Alert.alert('Sign Up Failed', err?.message || 'Something wrong')
            }
          })
          .catch(err => {
            console.log(err.message);
            Alert.alert('Sign Up Failed', err?.message || 'Something wrong')
          })
      },
    }),
    []
  );

  if (state.isLoading) {
    return <Text>Loading</Text>
  }

  return (
    <NavigationContainer>
      <AuthContext.Provider value={authContext}>
        <Stack.Navigator initialRouteName="Welcome" screenOptions={{ headerShown: false }}>
          {state.userToken === null ? (
            <>
              <Stack.Screen name="Welcome" component={WelcomeScreen} />
              <Stack.Screen name="Login" component={LoginScreen} />
              <Stack.Screen name="Register" component={RegisterScreen} />
            </>
          ) : (
            <>
              <Stack.Screen name="Home" component={HomeScreen} />
              <Stack.Screen name="PhotoStudio" component={PhotoStudio} />
              <Stack.Screen name="Photography" component={Photography} />
              <Stack.Screen name="Photography/About" component={AboutPhotography} />
              <Stack.Screen name="Recomendation" component={Recomendation} />
              <Stack.Screen name="Recomendation/Result" component={RecomendationResult} initialParams={{ userToken: state.userToken }} />
              <Stack.Screen name="Recomendation/Detail" component={RecomendationDetail} initialParams={{ userToken: state.userToken }} />
            </>
          )}
        </Stack.Navigator>
      </AuthContext.Provider>
    </NavigationContainer>
  )
}